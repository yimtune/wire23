package main

import "fmt"

func UseRepository() {
	repo := InitUserRepository() // 使用 wire_gen.go  生成代码里的方法
	fmt.Println(repo)
}

func main() {
	UseRepository()
}
