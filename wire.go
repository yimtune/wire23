//go:build wireinject

// 编译标签 "go:build wireinject"     让wire 命令行工具处理这个文件

package main

import (
	"gitee.com/yimtune/wire23/repository"
	"gitee.com/yimtune/wire23/repository/dao"
	"github.com/google/wire"
)

func InitUserRepository() *repository.UserRepository {
	// wire.Build 传入各种构造函数
	wire.Build(repository.NewUserRepository, InitDB, dao.NewUserDAO)
	//wire.Build(InitRedis, repository.NewUserRepository, InitDB, dao.NewUserDAO)
	//inject InitUserRepository: unused provider "wire.InitRedis" 没用上InitRedis
	return &repository.UserRepository{}
}
